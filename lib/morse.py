# Import the module for playing the sounds.
from pygame import mixer
# Impor time for waiting.
import time


class Morse(object):
    # Lookup table for the morse code for each character.
    morse_lut = {
        'A': '.-',      'B': '-...',
        'C': '-.-.',    'D': '-..',
        'E': '.',       'F': '..-.',
        'G': '--.',     'H': '....',
        'I': '..',      'J': '.---',
        'K': '-.-',     'L': '.-..',
        'M': '--',      'N': '-.',
        'O': '---',     'P': '.--.',
        'Q': '--.-',    'R': '.-.',
        'S': '...',     'T': '-',
        'U': '..-',     'V': '...-',
        'W': '.--',     'X': '-..-',
        'Y': '-.--',    'Z': '--..',
    }

    def __init__(self, sound_file: str = 'beep.wav', debug: bool = False):
        # Init the music player.
        mixer.init()
        # Load the given sound file.
        mixer.music.load(sound_file)

        self.debug = debug

    # PLay the beep for the given seconds.
    def playBeep(self, duration: int):
        # Start playing the sound.
        mixer.music.play()
        # Wait for the given duration.
        time.sleep(duration)
        # Stop playing the sound.
        mixer.music.stop()
        if self.debug:
            print("Beep for {} second".format(duration))

    def playMorseChar(self, char: str):
        # Find the code for the character in the lut.
        try:
            code = self.morse_lut[char]
        except KeyError as e:
            if self.debug:
                print('unrecognised character: {}'.format(char))
            return
        if self.debug:
            print('Char: {}; {}'.format(char, code))
        # For each symbol in the code, play a short or long beep.
        for fig in code:
            if fig == '.':
                # Short beep.
                self.playBeep(0.1)
                time.sleep(0.2)
            elif fig == '-':
                # Long beep.
                self.playBeep(0.4)
                time.sleep(0.2)
            else:
                # invalid character.
                pass

    def playMorseWord(self, word: str):
        if self.debug:
            print('Word: {}'.format(word))
        for char in word:
            # For each character in the word play the morse code.
            self.playMorseChar(char.upper())
            # Sleep an extra 0.25 seconds for a total of half a second.
            time.sleep(0.4)

    def playMorseByte(self, byte: int):
        """
        Plays a morse code, for a number between 0 and 255.

        Arguments:
            byte {int} -- The byte to be converted to morse
        """

        char = chr(int(map(byte, 0, 255, 65, 90)))
        self.playMorseChar(char)

    @staticmethod
    def map(value, startMin, startMax, endMin, endMax):
        # Figure out how 'wide' each range is
        leftSpan = startMax - startMin
        rightSpan = endMax - endMin

        # Convert the left range into a 0-1 range (float)
        valueScaled = float(value - startMin) / float(leftSpan)

        # Convert the 0-1 range into a value in the right range.
        return endMin + (valueScaled * rightSpan)


if __name__ == "__main__":
    code = Morse(debug=True)
    code.playMorseWord('AB')