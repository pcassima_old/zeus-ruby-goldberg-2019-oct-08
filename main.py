from lib.morse import Morse
import requests
import subprocess
import time

if __name__ == "__main__":
    # Testing.
    morseCodes = Morse(debug=True)
    primed = False
    print('Waiting for start...')
    # (MESSAGE)[0]{THIS IS THE PAYLOAD}
    while True:
        # Request the message page
        page = requests.get('http://kelder.zeus.ugent.be/messages')
        # Get the text from the webpage (the html content)
        content = page.text

        # Write this content to a file
        output = open('page.file', 'w')
        output.write(content)
        output.close()
        # Call a shell script to extract the message and get the result back
        result = subprocess.check_output('bash ./sol.sh', shell=True)
        # Decode the result and strip the newline
        result = result.decode('utf-8').strip()
        # result = '(MESSAGE)[5]{891234567891234567891234567891}'
        # If the message is in the right format
        if result == 'START RUBY' and not primed:
            print('PRIMED')
            primed = True
        elif ('(MESSAGE)' in result) and primed:
            # Start the link - request
            start_request = requests.get('http://10.1.0.155:5000/link/start/2/2')
            print('right format found')
            # Extract the payload
            result = result.split('{')[1][:-1]

            result = result[0:8]
            result2 = ''
            for char in result:
                result2 += chr(int(char) + 65)
            # Play the payload with morse codes
            morseCodes.playMorseWord(result2)
            # handoff the link request
            start_request = requests.get('http://10.1.0.155:5000/link/handoff/2/2')
            # Break the while loop
            break
        else:
            # If the message is not the right format, wait 5 seconds and try again.
            time.sleep(5)
        
